# React.js Application Deployment with GitLab CI and Surge

## Description

This repository demonstrates how to use GitLab CI to automate the deployment of a React.js application to Surge.

## Prerequisite

1. Ensure Node.js is installed.
2. Install Surge globally using the following command:

   ```sh
   npm install -g surge
   ```

## NOTE

To authenticate against Surge, follow these steps:

1. Generate a token locally by running:

   ```sh
   surge token
   ```

2. Log in to Surge by executing:

   ```sh
   surge login
   ```

3. Create two environment variables in your GitLab project settings:

   - `SURGE_LOGIN`: Your Surge account email used for logging in.
   - `SURGE_TOKEN`: The token generated in the previous step.
